# CASTLE Module
It represents the Castle register, which maintains castles data. It provides castle data management services, such as
a castle record creation and update, or search for castles based on input criteria. 

## How to run 
This module connects to the RabbitMQ broker. You can run it directly from the Java IDE with required parameters, or
you can build jar package and run it as `java -jar castle-<version>.jar <program parameters>` You can package the application
as the Docker image and run it inside container. 
 
* Program parameters: `--RABBITMQ_IP=192.168.99.100`

## Consul registry
If the application is properly shutdown, it is unregistered from Consul.
```
curl -X POST http://localhost:9194/actuator/shutdown
```

Otherwise, you can use Consul API to unregister service.
```
# Get details about service
curl http://192.168.122.230:8500/v1/catalog/service/castle
# create deregister.json request file with data like this:
# {
#   "Datacenter":"dc1",
#   "Node":"consul-server1",
#   "ServiceID":"castle-4f85018f00314d39a42fda17914d484e"
# }
# Deregister
curl -X PUT http://192.168.122.230:8500/v1/catalog/deregister --data-binary @deregister.json
# Or you can use consul command
consul services deregister -id=castle-c25a846a722337ac4dbbd78981bfd8ab-management
```