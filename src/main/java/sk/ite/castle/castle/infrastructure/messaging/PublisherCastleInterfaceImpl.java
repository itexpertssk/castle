package sk.ite.castle.castle.infrastructure.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;
import sk.ite.castle.castle.domain.event.CastleCreatedEvent;

/**
 * Created by macalaki on 31.01.2017.
 */

@Service
public class PublisherCastleInterfaceImpl implements PublisherCastleInterface {
    @Autowired
    private StreamBridge streamBridge;

    @Override
    public void sendCastleCreatedEvent(CastleCreatedEvent event) {
        streamBridge.send("castle-source", MessageBuilder.withPayload(event)
                .setHeader("SOME_HEADER", "value")
                .build());
//        publisher.sendCastleCreatedEvent().send(
//                MessageBuilder.withPayload(event)
//                        .setHeader("SOME_HEADER", "value")
//                        .build());
    }
}
