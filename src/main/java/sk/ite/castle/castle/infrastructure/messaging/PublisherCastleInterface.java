package sk.ite.castle.castle.infrastructure.messaging;

import sk.ite.castle.castle.domain.event.CastleCreatedEvent;

/**
 * Created by macalaki on 31.01.2017.
 */
public interface PublisherCastleInterface {
    void sendCastleCreatedEvent(CastleCreatedEvent event);
}
