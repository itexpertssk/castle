package sk.ite.castle.castle.domain.event;

/**
 * Created by macalaki on 31.01.2017.
 */
public class CastleCreatedEvent {
    private Long id;
    private String name;
    private String ruler;
    private String location;

    public CastleCreatedEvent(Long id, String name, String ruler, String location) {
        this.id = id;
        this.name = name;
        this.ruler = ruler;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRuler() {
        return ruler;
    }

    public String getLocation() {
        return location;
    }
}
