package sk.ite.castle.castle.application.service;

import sk.ite.castle.castle.application.dto.DTOCastle;

import java.util.List;

/**
 * CastleService interface.
 *
 * @author macalak@itexperts.sk
 *
 */
public interface CastleService {
	
	List<DTOCastle> getCastles();
	void createCastle(DTOCastle castle);

}
