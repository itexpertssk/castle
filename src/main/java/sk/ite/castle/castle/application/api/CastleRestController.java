package sk.ite.castle.castle.application.api;


import org.springframework.web.bind.annotation.RequestBody;
import sk.ite.castle.castle.application.dto.DTOCastle;
import sk.ite.castle.castle.application.service.CastleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * JAX-RS wrapper around CastleService.
 *
 * @author macalak@itexperts.sk
 *
 */
@RestController
public class CastleRestController {
	final Logger LOGGER = LoggerFactory.getLogger(CastleRestController.class);
    
    @Autowired
    CastleService castleService;
    
    @RequestMapping(produces={"application/json"}, value="/castles", method=RequestMethod.GET )
    public List<DTOCastle> getCastles() {
    	LOGGER.info("Getting all Castles from catalogue...");
        return castleService.getCastles();
    }

    @RequestMapping(consumes={"application/json"}, value="/castles", method=RequestMethod.POST )
    public void createCastle(@RequestBody DTOCastle castle) {
    	LOGGER.info("Creating new castle ...");
        castleService.createCastle(castle);
    }
}
